class CreateGalleryImages < ActiveRecord::Migration
  def change
    create_table :gallery_images do |t|
      t.references :gallery, index: true
      t.attachment :image

      t.timestamps
    end
  end
end
