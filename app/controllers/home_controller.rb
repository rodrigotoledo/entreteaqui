class HomeController < ApplicationController
  def index
    @slider_banners = Banner.active.where(slider: true).sorted
    @top_banner = Banner.active.where(top: true).first
    @top_small_banners = Banner.active.where(top_small: true).sorted

    @last_news = News.active.limit(8).sorted

    @last_galleries = Gallery.active.limit(4).sorted
    @top_galleries = Gallery.active.limit(8).order(views: :asc, occur_at: :desc)

    @next_event = Event.active.sorted.first
    @top_events = Event.active.limit(8).order(views: :asc, occur_at: :desc)

    @last_videos = Video.active.limit(6).sorted

    @last_companies = Company.active.limit(4).order(created_at: :desc)
  end
end
