class FrontNewsController < ApplicationController
  def index
    @news = News.active.sorted
  end

  def show
    @news = News.friendly.find(params[:id])
  end

  def by_author
    @author = Author.friendly.find(params[:author_id])
    @news = @author.news.active.sorted
  end
end
