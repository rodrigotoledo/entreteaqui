class FrontCompaniesController < ApplicationController
  def index
    @companies = Company.active.order(:name)
  end
end
