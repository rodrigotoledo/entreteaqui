class FrontTalkWithUsController < ApplicationController
  def index
  end

  def deliver
    if ContactMailer.new_contact(params[:contact]).deliver
      flash[:success] = 'Sua mensagem foi enviada com sucesso. Em breve entraremos em contato com a resposta'
    else
      flash[:error] = 'Sua mensagem não pode ser enviada pois os parâmetros estão incorretos.'
    end
    redirect_to front_talk_with_us_path
  end
end
