class ContactMailer < ActionMailer::Base
  layout 'email'
  default from: "no-reply@entreteaqui.com.br"
  helper :application
  DEFAULT_TO = "contato@entreteaqui.com.br"

  def new_contact(mail_attributes)
    @name    = mail_attributes[:name]
    @subject = mail_attributes[:subject]
    @email   = mail_attributes[:email]
    @message = mail_attributes[:message]
    @company = mail_attributes[:company]

    mail to: ContactMailer::DEFAULT_TO, reply_to: @email
  end
end
