json.array!(@authors) do |author|
  json.extract! author, :id, :name, :about
  json.url author_url(author, format: :json)
end
