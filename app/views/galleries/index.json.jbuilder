json.array!(@galleries) do |gallery|
  json.extract! gallery, :id, :event_id, :title, :image, :description, :address, :occur_at, :active, :views, :slug
  json.url gallery_url(gallery, format: :json)
end
