json.array!(@videos) do |video|
  json.extract! video, :id, :title, :singer, :youtube_code, :active
  json.url video_url(video, format: :json)
end
