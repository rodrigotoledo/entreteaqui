module GalleriesHelper
  def images_in_directories
    files = Dir.entries(File.join(Rails.root,'fotos'))
    images = []
    files.sort!
    files.each do |f|
      full_path = File.join(Rails.root,'fotos',f)
      next if f == '.' || f == '..' || !File.directory?(full_path)

      directory_with_images = File.join(Rails.root,'fotos',f,"*.{jpg,jpeg,JPG,JPEG}")
      files = Dir.glob(directory_with_images)
      images << ["#{f} - #{files.size} fotos",f]
    end
    images
  end
end
