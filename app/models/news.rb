class News < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :author
  belongs_to :news_category

  has_attached_file :image, styles: {thumb: '64x57#', thumb_top: '67x54#', next: '184x119#', head: '670', big: '1024'}, default_style: :thumb,convert_options: {head: "-quality 90 -strip", next: "-quality 90 -strip", big: "-quality 90 -strip"}


  validates :title, :description, :news_category_id, :author_id, presence: true
  validates_attachment_presence :image

  def self.active
    where(active: true)
  end

  def self.sorted
    order(occur_at: :desc)
  end
end
