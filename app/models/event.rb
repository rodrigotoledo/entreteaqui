class Event < ActiveRecord::Base
  has_many :galleries
  has_many :gallery_images, through: :galleries

  extend FriendlyId
  friendly_id :title, use: :slugged

  has_attached_file :image, styles: {thumb: '64x57#', thumb_top: '67x54#', next: '184x119#', head: '670', big: '1024'}, default_style: :thumb,convert_options: {head: "-quality 90 -strip", next: "-quality 90 -strip", big: "-quality 90 -strip"}
  validates_attachment_presence :image
  # do_not_validate_attachment_file_type :image

  validates :title, :address, :description, presence: true

  def self.active
    where(active: true)
  end

  def self.sorted
    order(occur_at: :desc)
  end
end
