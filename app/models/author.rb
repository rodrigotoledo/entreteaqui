class Author < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :news

  validates :name, :about, presence: true

  def self.sorted_by_count
    joins('left join news on authors.id = news.author_id').group("news.author_id").order("count(news.author_id) desc")
  end

  def self.sorted
    order(name: :desc)
  end
end
