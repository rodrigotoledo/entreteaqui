class Banner < ActiveRecord::Base

  has_attached_file :image, styles: {slider: '470x409#', top: '210x409#',
    top_small: '295x131#', header: '602x68#'}, default_style: :slider,
    convert_options: {slider: "-quality 90 -strip", top: "-quality 90 -strip",
      top_small: "-quality 90 -strip", header: "-quality 90 -strip"}
  do_not_validate_attachment_file_type :image

  def self.active
    where(active: true)
  end

  def self.sorted
    order(created_at: :desc)
  end
end
